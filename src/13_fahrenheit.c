#include <stdio.h>

int main() {
    float fahr, celsius;
    float lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;

    fahr = lower;
    printf("%3s %6s\n", "oC", "Fahren");
    printf("%3s %6s\n", "===", "======");
    while(fahr<= upper){
        celsius = 5.0 * (fahr -32 ) /9.0;
        printf("%3.0f %6.1f\n", fahr, celsius);
        fahr = fahr + step;
    }
    return 0;
}
